// The colors used to gradient the buttons based on correctness scores.
// They need to be sorted with the most wrong color first, and the most right color last.
// The current schema is bad=red, good=blue
colors = ["#800000","#990000","#b10000", "#cc0000", "#e60000", "#ff0000", "#ff1a1a", "#ff3333", "#ff4d4d", "#ff6666",
					"#6666ff", "#4d4dff", "#3333ff", "#1a1aff", "#0000ff", "#0000e6", "#0000cc", "#0000b3", "#000099", "#000080"]

