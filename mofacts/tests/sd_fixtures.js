SDTDF = function() { return {
    "_id" : "rgPyyfRar5x4rxQSy",
    "fileName" : "SDTrainTest.xml",
    "tdfs" : {
        "tutor" : {
            "setspec" : [
                {
                    "name" : [
                        "SDTrainTest"
                    ],
                    "lessonname" : [
                        "SDTrainTest"
                    ],
                    "stimulusfile" : [
                        "SDTrainTestStims.xml"
                    ],
                    "clustermodel" : [
                        ""
                    ],
                    "clustersize" : [
                        "1"
                    ],
                    "xshuffleclusters" : [
                        "0-30"
                    ],
                    "lfparameter" : [
                        ".85"
                    ],
                    "isModeled" : [
                        "false"
                    ]
                }
            ],
            "unit" : [
                {
                    "unitname" : [
                        "Introduction"
                    ],
                    "unitinstructions" : [
                        "This is a system testing module for Cloze trials"
                    ]
                },
                {
                    "unitname" : [
                        "Statistic SD Pre-Study"
                    ],
                    "unitinstructions" : [
                        "Please study\n        "
                    ],
                    "deliveryparams" : [
                        {
                            "purestudy" : [
                                "10000"
                            ],
                            "skipstudy" : [
                                "true"
                            ],
                            "reviewstudy" : [
                                "1"
                            ],
                            "correctprompt" : [
                                "1"
                            ]
                        }
                    ],
                    "assessmentsession" : [
                        {
                            "conditiontemplatesbygroup" : [
                                {
                                    "groupnames" : [
                                        "A"
                                    ],
                                    "clustersrepeated" : [
                                        "1"
                                    ],
                                    "templatesrepeated" : [
                                        "31"
                                    ],
                                    "group" : [
                                        "\n                    r,f,s,0 r,f,s,0 r,f,s,0 r,f,s,0\n                    r,f,s,0 r,f,s,0 r,f,s,0 r,f,s,0\n                    r,f,s,0 r,f,s,0 r,f,s,0 r,f,s,0\n                    r,f,s,0 r,f,s,0 r,f,s,0 r,f,s,0\n                    r,f,s,0 r,f,s,0 r,f,s,0 r,f,s,0\n                    r,f,s,0 r,f,s,0 r,f,s,0 r,f,s,0\n                    r,f,s,0 r,f,s,0 r,f,s,0 r,f,s,0\n                    r,f,s,0 r,f,s,0 r,f,s,0\n                "
                                    ]
                                }
                            ],
                            "initialpositions" : [
                                "\n                A_1  A_2  A_3  A_4  A_5\n                A_6  A_7  A_8  A_9  A_10\n                A_11 A_12 A_13 A_14 A_15\n                A_16 A_17 A_18 A_19 A_20\n                A_21 A_22 A_23 A_24 A_25\n                A_26 A_27 A_28 A_29 A_30\n                A_31\n            "
                            ],
                            "randomizegroups" : [
                                "false"
                            ],
                            "clusterlist" : [
                                "0-30"
                            ],
                            "permutefinalresult" : [
                                ""
                            ],
                            "assignrandomclusters" : [
                                "false"
                            ],
                            "randomchoices" : [
                                "2"
                            ]
                        }
                    ]
                },
                {
                    "unitname" : [
                        "Statistic SD Unit"
                    ],
                    "unitinstructions" : [
                        "Statistics Memory Experiment\n\n            Task: Please fill in the missing word or hit ENTER to see the fill in.\n\n            Goal: Type the missing word within 12 seconds to score a point.\n        "
                    ],
                    "deliveryparams" : [
                        {
                            "purestudy" : [
                                "1"
                            ],
                            "skipstudy" : [
                                "false"
                            ],
                            "reviewstudy" : [
                                "5000"
                            ],
                            "correctprompt" : [
                                "500"
                            ]
                        }
                    ],
                    "assessmentsession" : [
                        {
                            "conditiontemplatesbygroup" : [
                                {
                                    "groupnames" : [
                                        "A"
                                    ],
                                    "clustersrepeated" : [
                                        "1"
                                    ],
                                    "templatesrepeated" : [
                                        "31"
                                    ],
                                    "group" : [
                                        "\n                    r,b,d,0 r,b,d,0 r,b,d,0 r,b,d,0\n                    r,b,d,0 r,b,d,0 r,b,d,0 r,b,d,0\n                    r,b,d,0 r,b,d,0 r,b,d,0 r,b,d,0\n                    r,b,d,0 r,b,d,0 r,b,d,0 r,b,d,0\n                    r,b,d,0 r,b,d,0 r,b,d,0 r,b,d,0\n                    r,b,d,0 r,b,d,0 r,b,d,0 r,b,d,0\n                    r,b,d,0 r,b,d,0 r,b,d,0 r,b,d,0\n                    r,b,d,0 r,b,d,0 r,b,d,0\n                "
                                    ]
                                }
                            ],
                            "initialpositions" : [
                                "\n                A_1  A_2  A_3  A_4  A_5\n                A_6  A_7  A_8  A_9  A_10\n                A_11 A_12 A_13 A_14 A_15\n                A_16 A_17 A_18 A_19 A_20\n                A_21 A_22 A_23 A_24 A_25\n                A_26 A_27 A_28 A_29 A_30\n                A_31\n            "
                            ],
                            "randomizegroups" : [
                                "false"
                            ],
                            "clusterlist" : [
                                "0-30"
                            ],
                            "permutefinalresult" : [
                                ""
                            ],
                            "assignrandomclusters" : [
                                "false"
                            ],
                            "randomchoices" : [
                                "2"
                            ]
                        }
                    ]
                },
                {
                    "unitname" : [
                        "Final"
                    ],
                    "unitinstructions" : [
                        "Thank you for testing"
                    ]
                }
            ]
        }
    }
};
};

SDStims = function() { return {
    "_id" : "hbsoygpjeP6gxbLJ6",
    "fileName" : "SDTrainTestStims.xml",
    "stimuli" : {
        "setspec" : {
            "clusters" : [
                {
                    "cluster" : [
                        {
                            "displayType" : [
                                "Cloze"
                            ],
                            "display" : [
                                "A _______ is a small part or quantity intended to show what the whole, or population,<br>is like.",
                                "A sample is a small part or quantity intended to show what the _______, or<br>population, is like."
                            ],
                            "response" : [
                                "sample",
                                "whole"
                            ]
                        },
                        {
                            "displayType" : [
                                "Cloze"
                            ],
                            "display" : [
                                "A sample is meant to _______ the quality or style of the entity, or population,<br>from which it came.",
                                "A sample is meant to represent the quality or style of the entity, or _______,<br>from which it came."
                            ],
                            "response" : [
                                "represent",
                                "population"
                            ]
                        },
                        {
                            "displayType" : [
                                "Cloze"
                            ],
                            "display" : [
                                "The sample size is the number of _______ you took from your population.",
                                "The sample size is the number of observations you took from your _______."
                            ],
                            "response" : [
                                "observations",
                                "population"
                            ]
                        },
                        {
                            "displayType" : [
                                "Cloze"
                            ],
                            "display" : [
                                "When your study involves human subjects, your _______ is the group of people<br>who participated in the study.",
                                "When your study involves human subjects, your sample is the _______ of people<br>who participated in the study."
                            ],
                            "response" : [
                                "sample",
                                "group"
                            ]
                        },
                        {
                            "displayType" : [
                                "Cloze"
                            ],
                            "display" : [
                                "We use the term _______ to refer to how different, or how diverse, are the<br>data in your sample.",
                                "We use the term variability to refer to how _______, or how diverse, are the<br>data in your sample."
                            ],
                            "response" : [
                                "variability",
                                "different"
                            ]
                        },
                        {
                            "displayType" : [
                                "Cloze"
                            ],
                            "display" : [
                                "A _______ is one of many different ways we can describe traits of our sample.",
                                "A statistic is one of many different ways we can describe traits of our _______."
                            ],
                            "response" : [
                                "statistic",
                                "sample"
                            ]
                        },
                        {
                            "displayType" : [
                                "Cloze"
                            ],
                            "display" : [
                                "Examples of _______ are the mean, standard deviation, and variance; each term<br>has its own use (or several uses) in summarizing a set of observations.",
                                "Examples of statistics are the mean, standard deviation, and variance; each<br>term has its own use (or several uses) in summarizing a set of _______."
                            ],
                            "response" : [
                                "statistics",
                                "observations"
                            ]
                        },
                        {
                            "displayType" : [
                                "Cloze"
                            ],
                            "display" : [
                                "The statistical term for the \"_______\" is called the \"mean.\"",
                                "The statistical term for the \"average\" is called the \"_______.\""
                            ],
                            "response" : [
                                "average",
                                "mean"
                            ]
                        },
                        {
                            "displayType" : [
                                "Cloze"
                            ],
                            "display" : [
                                "The _______ is one number that represents a trait of the most typical member<br>in the sample.",
                                "The mean is one number that represents a trait of the most _______ member in<br>the sample."
                            ],
                            "response" : [
                                "mean",
                                "typical"
                            ]
                        },
                        {
                            "displayType" : [
                                "Cloze"
                            ],
                            "display" : [
                                "When you _______ all the numbers in a set and then divide by the amount of<br>numbers you have, you get a statistic called the mean.",
                                "When you sum all the numbers in a set and then divide by the amount of numbers<br>you have, you get a statistic called the _______."
                            ],
                            "response" : [
                                "sum",
                                "mean"
                            ]
                        },
                        {
                            "displayType" : [
                                "Cloze"
                            ],
                            "display" : [
                                "_______ and standard deviation are ways to conceptualize variability.",
                                "Variance and _______ are ways to conceptualize variability."
                            ],
                            "response" : [
                                "variance",
                                "standard deviation"
                            ]
                        },
                        {
                            "displayType" : [
                                "Cloze"
                            ],
                            "display" : [
                                "The term \"difference from the mean\" is the resulting number when you _______<br>a certain observation from the average of that sample.",
                                "The term \"difference from the mean\" is the resulting number when you subtract<br>a certain observation from the _______ of that sample."
                            ],
                            "response" : [
                                "subtract",
                                "average"
                            ]
                        },
                        {
                            "displayType" : [
                                "Cloze"
                            ],
                            "display" : [
                                "We are studying weight. In order to calculate Bob's difference from the mean,<br>we subtract Bob's _______ weight from the average weight.",
                                "We are studying weight. In order to calculate Bob's difference from the mean,<br>we subtract Bob's observed weight from the _______ weight."
                            ],
                            "response" : [
                                "observed",
                                "average"
                            ]
                        },
                        {
                            "displayType" : [
                                "Cloze"
                            ],
                            "display" : [
                                "When you _______ the difference between one score and the mean score, you are<br>computing the variance for that measurement",
                                "When you square the difference between one score and the mean score, you are<br>computing the _______ for that measurement"
                            ],
                            "response" : [
                                "square",
                                "variance"
                            ]
                        },
                        {
                            "displayType" : [
                                "Cloze"
                            ],
                            "display" : [
                                "When you sum the variance for the all of your measurements, you get the sum<br>of _______ from the mean in that sample.",
                                "When you sum the variance for the all of your measurements, you get the sum<br>of squared differences from the _______ in that sample."
                            ],
                            "response" : [
                                "squared differences",
                                "mean"
                            ]
                        },
                        {
                            "displayType" : [
                                "Cloze"
                            ],
                            "display" : [
                                "In a typical sample, some observations are _______ the mean, and others are<br>above the mean.",
                                "In a typical sample, some observations are below the mean, and others are _______<br>the mean."
                            ],
                            "response" : [
                                "below",
                                "above"
                            ]
                        },
                        {
                            "displayType" : [
                                "Cloze"
                            ],
                            "display" : [
                                "In calculating the variance for a sample, you must _______ the differences<br>from the mean before add them.",
                                "In calculating the variance for a sample, you must square the differences from<br>the mean before _______ them."
                            ],
                            "response" : [
                                "square",
                                "add"
                            ]
                        },
                        {
                            "displayType" : [
                                "Cloze"
                            ],
                            "display" : [
                                "While calculating variance, one reason we add the _______ differences from<br>the mean, and not the actual differences, is that some of the latter type<br>will be positive, and others negative.",
                                "While calculating variance, one reason we add the squared differences from<br>the mean, and not the _______ differences, is that some of the latter type<br>will be positive, and others negative."
                            ],
                            "response" : [
                                "squared",
                                "actual"
                            ]
                        },
                        {
                            "displayType" : [
                                "Cloze"
                            ],
                            "display" : [
                                "If you were to add the _______ differences from the mean, the positive and<br>negative numbers would cancel out and underestimate the variability of the<br>sample.",
                                "If you were to add the actual differences from the mean, the positive and negative<br>numbers would cancel out and _______ the variability of the sample."
                            ],
                            "response" : [
                                "actual",
                                "underestimate"
                            ]
                        },
                        {
                            "displayType" : [
                                "Cloze"
                            ],
                            "display" : [
                                "The \"sum of squared errors,\" which is often abbreviated to \"sum of squares,\"<br>is what happens when you add all the squared _______ from the mean.",
                                "The \"sum of squared errors,\" which is often abbreviated to \"sum of squares,\"<br>is what happens when you add all the squared differences from the _______."
                            ],
                            "response" : [
                                "differences",
                                "mean"
                            ]
                        },
                        {
                            "displayType" : [
                                "Cloze"
                            ],
                            "display" : [
                                "You get the _______ of a sample when you divide the sum of squared errors by<br>the number that is one fewer than the total number observations.",
                                "You get the variance of a sample when you divide the sum of squared errors<br>by the number that is one _______ than the total number observations."
                            ],
                            "response" : [
                                "variance",
                                "fewer"
                            ]
                        },
                        {
                            "displayType" : [
                                "Cloze"
                            ],
                            "display" : [
                                "If the sum of squared errors in a dataset is 100 and that dataset had 11 _______,<br>then you divide 100 by one less than 11, or 100/(11-1), or 100/10, or 10<br>to get the variance.",
                                "If the sum of squared errors in a dataset is 100 and that dataset had 11 observations,<br>then you divide 100 by one less than 11, or 100/(11-1), or 100/10, or 10<br>to get the _______."
                            ],
                            "response" : [
                                "observations",
                                "variance"
                            ]
                        },
                        {
                            "displayType" : [
                                "Cloze"
                            ],
                            "display" : [
                                "The _______ of the variance is the standard deviation",
                                "The square root of the variance is the _______"
                            ],
                            "response" : [
                                "square root",
                                "standard deviation"
                            ]
                        },
                        {
                            "displayType" : [
                                "Cloze"
                            ],
                            "display" : [
                                "One reason we do not typically work with variances is that, in _______ the<br>differences from the mean prior to adding them, we are now working with<br>a different metric from the observations.",
                                "One reason we do not typically work with variances is that, in squaring the<br>differences from the mean prior to adding them, we are now working with<br>a different _______ from the observations."
                            ],
                            "response" : [
                                "squaring",
                                "metric"
                            ]
                        },
                        {
                            "displayType" : [
                                "Cloze"
                            ],
                            "display" : [
                                "Squaring a value also changes the _______. For example, squaring something<br>measured in pounds will change the unit of measurement to \"pounds squared.\"",
                                "Squaring a value also changes the metric. For example, squaring something measured<br>in pounds will change the unit of measurement to \"pounds _______.\""
                            ],
                            "response" : [
                                "metric",
                                "squared"
                            ]
                        },
                        {
                            "displayType" : [
                                "Cloze"
                            ],
                            "display" : [
                                "The variance is usually too big for descriptive purposes because it is the<br>_______ not of actual differences from the mean but of the squares of those<br>differences.",
                                "The variance is usually too big for descriptive purposes because it is the<br>sum not of actual differences from the mean but of the _______ of those<br>differences."
                            ],
                            "response" : [
                                "sum",
                                "squares"
                            ]
                        },
                        {
                            "displayType" : [
                                "Cloze"
                            ],
                            "display" : [
                                "Taking the square root of the variance will _______ the value, thus undoing<br>the inflation that occurred when we added the squared differences.",
                                "Taking the square root of the variance will shrink the value, thus undoing<br>the _______ that occurred when we added the squared differences."
                            ],
                            "response" : [
                                "shrink",
                                "inflation"
                            ]
                        },
                        {
                            "displayType" : [
                                "Cloze"
                            ],
                            "display" : [
                                "Taking the square root of the variance will revert the value back to the same<br>_______ as your observations.",
                                "Taking the square root of the variance will revert the value back to the same<br>metric as your _______."
                            ],
                            "response" : [
                                "metric",
                                "observations"
                            ]
                        },
                        {
                            "displayType" : [
                                "Cloze"
                            ],
                            "display" : [
                                "A standard deviation can be thought of as one number that tells you, on average,<br>how far off the _______ observation is from the average of the sample.",
                                "A standard deviation can be thought of as one number that tells you, on _______,<br>how far off the typical observation is from the average of the sample."
                            ],
                            "response" : [
                                "typical",
                                "average"
                            ]
                        },
                        {
                            "displayType" : [
                                "Cloze"
                            ],
                            "display" : [
                                "Although statistics can be intimidating, the term \"standard deviation\" simply<br>refers to the _______ (or \"standard\") difference (or \"deviation\") between<br>any given observation and the average score",
                                "Although statistics can be intimidating, the term \"standard deviation\" simply<br>refers to the typical (or \"standard\") difference (or \"deviation\") between<br>any given observation and the _______ score"
                            ],
                            "response" : [
                                "typical",
                                "average"
                            ]
                        },
                        {
                            "displayType" : [
                                "Cloze"
                            ],
                            "display" : [
                                "_______ is a convenient way to discuss the typical difference from the sample<br>mean because it is in the same metric as the observations.",
                                "Standard deviation is a convenient way to discuss the typical difference from<br>the sample mean because it is in the same _______ as the observations."
                            ],
                            "response" : [
                                "standard deviation",
                                "metric"
                            ]
                        }
                    ]
                }
            ]
        }
    }
};
};
