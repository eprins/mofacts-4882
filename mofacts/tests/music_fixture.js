//Fixtures used for testing

MusicTDF = function() { return {
    "_id" : "gGdgRwES64L85mfaa",
    "fileName" : "Music2-0.xml",
    "tdfs" : {
        "tutor" : {
            "setspec" : [
                {
                    "name" : [
                        "Harmony1"
                    ],
                    "lessonname" : [
                        "Music2-0"
                    ],
                    "userselect" : [
                        "false"
                    ],
                    "stimulusfile" : [
                        "Music2stim.xml"
                    ],
                    "clustermodel" : [
                        ""
                    ],
                    "clustersize" : [
                        "1"
                    ],
                    "isModeled" : [
                        "false"
                    ],
                    "buttonorder" : [
                        "Octave, Major 7th, Perfect 5th, Tritone, Major 3rd, Minor 3rd"
                    ]
                }
            ],
            "unit" : [
                {
                    "unitname" : [
                        "Introduction"
                    ],
                    "picture" : [
                        "/resources/images/IntervalInstructions.jpg"
                    ]
                },
                {
                    "unitname" : [
                        "Minor 3rd"
                    ],
                    "unitinstructions" : [
                        " Click the OK button to hear one example of a minor 3rd as a harmony and as a melody (not necessarily in that order).\n        "
                    ],
                    "deliveryparams" : [
                        {
                            "purestudy" : [
                                "3000"
                            ],
                            "readyprompt" : [
                                "0"
                            ],
                            "reviewstudy" : [
                                "3000"
                            ],
                            "correctprompt" : [
                                "1000"
                            ],
                            "drill" : [
                                "10000"
                            ]
                        }
                    ],
                    "buttontrial" : [
                        "true"
                    ],
                    "assessmentsession" : [
                        {
                            "conditiontemplatesbygroup" : [
                                {
                                    "groupnames" : [
                                        "A"
                                    ],
                                    "clustersrepeated" : [
                                        "1"
                                    ],
                                    "templatesrepeated" : [
                                        "2"
                                    ],
                                    "group" : [
                                        "r,f,s,0 r,f,s,0"
                                    ]
                                }
                            ],
                            "initialpositions" : [
                                "\n                A_1 A_2\n            "
                            ],
                            "randomchoices" : [
                                "3"
                            ],
                            "randomizegroups" : [
                                "false"
                            ],
                            "clusterlist" : [
                                "0-0 6-6"
                            ],
                            "assignrandomclusters" : [
                                "false"
                            ],
                            "permutefinalresult" : [
                                "0-1"
                            ]
                        }
                    ]
                },
                {
                    "unitname" : [
                        "Major 3rd"
                    ],
                    "unitinstructions" : [
                        " Click the OK button to hear one example of a major 3rd as a harmony and as a melody (not necessarily in that order).\n        "
                    ],
                    "deliveryparams" : [
                        {
                            "purestudy" : [
                                "3000"
                            ],
                            "readyprompt" : [
                                "0"
                            ],
                            "reviewstudy" : [
                                "3000"
                            ],
                            "correctprompt" : [
                                "1000"
                            ],
                            "drill" : [
                                "10000"
                            ]
                        }
                    ],
                    "buttontrial" : [
                        "true"
                    ],
                    "assessmentsession" : [
                        {
                            "conditiontemplatesbygroup" : [
                                {
                                    "groupnames" : [
                                        "A"
                                    ],
                                    "clustersrepeated" : [
                                        "1"
                                    ],
                                    "templatesrepeated" : [
                                        "2"
                                    ],
                                    "group" : [
                                        "r,f,s,0 r,f,s,0"
                                    ]
                                }
                            ],
                            "initialpositions" : [
                                "\n                A_1 A_2\n            "
                            ],
                            "randomchoices" : [
                                "3"
                            ],
                            "randomizegroups" : [
                                "false"
                            ],
                            "clusterlist" : [
                                "1-1 7-7"
                            ],
                            "assignrandomclusters" : [
                                "false"
                            ],
                            "permutefinalresult" : [
                                "0-1"
                            ]
                        }
                    ]
                },
                {
                    "unitname" : [
                        "Tritone"
                    ],
                    "unitinstructions" : [
                        " Click the OK button to hear one example of a tritone as a harmony and as a melody (not necessarily in that order).\n        "
                    ],
                    "deliveryparams" : [
                        {
                            "purestudy" : [
                                "3000"
                            ],
                            "readyprompt" : [
                                "0"
                            ],
                            "reviewstudy" : [
                                "3000"
                            ],
                            "correctprompt" : [
                                "1000"
                            ],
                            "drill" : [
                                "10000"
                            ]
                        }
                    ],
                    "buttontrial" : [
                        "true"
                    ],
                    "assessmentsession" : [
                        {
                            "conditiontemplatesbygroup" : [
                                {
                                    "groupnames" : [
                                        "A"
                                    ],
                                    "clustersrepeated" : [
                                        "1"
                                    ],
                                    "templatesrepeated" : [
                                        "2"
                                    ],
                                    "group" : [
                                        "r,f,s,0 r,f,s,0"
                                    ]
                                }
                            ],
                            "initialpositions" : [
                                "\n                A_1 A_2\n            "
                            ],
                            "randomchoices" : [
                                "3"
                            ],
                            "randomizegroups" : [
                                "false"
                            ],
                            "clusterlist" : [
                                "2-2 8-8"
                            ],
                            "assignrandomclusters" : [
                                "false"
                            ],
                            "permutefinalresult" : [
                                "0-1"
                            ]
                        }
                    ]
                },
                {
                    "unitname" : [
                        "Perfect 5th"
                    ],
                    "unitinstructions" : [
                        " Click the OK button to hear one example of a perfect 5th as a harmony and as a melody (not necessarily in that order).\n        "
                    ],
                    "deliveryparams" : [
                        {
                            "purestudy" : [
                                "3000"
                            ],
                            "readyprompt" : [
                                "0"
                            ],
                            "reviewstudy" : [
                                "3000"
                            ],
                            "correctprompt" : [
                                "1000"
                            ],
                            "drill" : [
                                "10000"
                            ]
                        }
                    ],
                    "buttontrial" : [
                        "true"
                    ],
                    "assessmentsession" : [
                        {
                            "conditiontemplatesbygroup" : [
                                {
                                    "groupnames" : [
                                        "A"
                                    ],
                                    "clustersrepeated" : [
                                        "1"
                                    ],
                                    "templatesrepeated" : [
                                        "2"
                                    ],
                                    "group" : [
                                        "r,f,s,0 r,f,s,0"
                                    ]
                                }
                            ],
                            "initialpositions" : [
                                "\n                A_1 A_2\n            "
                            ],
                            "randomchoices" : [
                                "3"
                            ],
                            "randomizegroups" : [
                                "false"
                            ],
                            "clusterlist" : [
                                "3-3 9-9"
                            ],
                            "assignrandomclusters" : [
                                "false"
                            ],
                            "permutefinalresult" : [
                                "0-1"
                            ]
                        }
                    ]
                },
                {
                    "unitname" : [
                        "Major 7th"
                    ],
                    "unitinstructions" : [
                        " Click the OK button to hear one example of a major 7th as a harmony and as a melody (not necessarily in that order).\n        "
                    ],
                    "deliveryparams" : [
                        {
                            "purestudy" : [
                                "3000"
                            ],
                            "readyprompt" : [
                                "0"
                            ],
                            "reviewstudy" : [
                                "3000"
                            ],
                            "correctprompt" : [
                                "1000"
                            ],
                            "drill" : [
                                "10000"
                            ]
                        }
                    ],
                    "buttontrial" : [
                        "true"
                    ],
                    "assessmentsession" : [
                        {
                            "conditiontemplatesbygroup" : [
                                {
                                    "groupnames" : [
                                        "A"
                                    ],
                                    "clustersrepeated" : [
                                        "1"
                                    ],
                                    "templatesrepeated" : [
                                        "2"
                                    ],
                                    "group" : [
                                        "r,f,s,0 r,f,s,0"
                                    ]
                                }
                            ],
                            "initialpositions" : [
                                "\n                A_1 A_2\n            "
                            ],
                            "randomchoices" : [
                                "3"
                            ],
                            "randomizegroups" : [
                                "false"
                            ],
                            "clusterlist" : [
                                "4-4 10-10"
                            ],
                            "assignrandomclusters" : [
                                "false"
                            ],
                            "permutefinalresult" : [
                                "0-1"
                            ]
                        }
                    ]
                },
                {
                    "unitname" : [
                        "Octave"
                    ],
                    "unitinstructions" : [
                        " Click the OK button to hear one example of an octave as a harmony and as a melody (not necessarily in that order).\n        "
                    ],
                    "deliveryparams" : [
                        {
                            "purestudy" : [
                                "3000"
                            ],
                            "readyprompt" : [
                                "0"
                            ],
                            "reviewstudy" : [
                                "3000"
                            ],
                            "correctprompt" : [
                                "1000"
                            ],
                            "drill" : [
                                "10000"
                            ]
                        }
                    ],
                    "buttontrial" : [
                        "true"
                    ],
                    "assessmentsession" : [
                        {
                            "conditiontemplatesbygroup" : [
                                {
                                    "groupnames" : [
                                        "A"
                                    ],
                                    "clustersrepeated" : [
                                        "1"
                                    ],
                                    "templatesrepeated" : [
                                        "2"
                                    ],
                                    "group" : [
                                        "r,f,s,0 r,f,s,0"
                                    ]
                                }
                            ],
                            "initialpositions" : [
                                "\n                A_1 A_2\n            "
                            ],
                            "randomchoices" : [
                                "3"
                            ],
                            "randomizegroups" : [
                                "false"
                            ],
                            "clusterlist" : [
                                "5-5 11-11"
                            ],
                            "assignrandomclusters" : [
                                "false"
                            ],
                            "permutefinalresult" : [
                                "0-1"
                            ]
                        }
                    ]
                },
                {
                    "unitname" : [
                        "Musical Interval Study Pretest Harmony1"
                    ],
                    "unitinstructions" : [
                        "Pretest portion (72 sounds)\n\n            Listen to each sound, then click on the name of the interval you just heard. After a correct response, you will be shown the next interval. After each incorrect response, you will be provided with review to help you learn.\n\n            This pretest will get an initial measure of your skill in the task, and will be followed by 108 practice sounds, and (either 2 minutes, 1 day, or 7 days later) a posttest of 72 sounds.\n        "
                    ],
                    "deliveryparams" : [
                        {
                            "purestudy" : [
                                "3000"
                            ],
                            "readyprompt" : [
                                "0"
                            ],
                            "reviewstudy" : [
                                "3000"
                            ],
                            "correctprompt" : [
                                "1000"
                            ],
                            "drill" : [
                                "10000"
                            ]
                        }
                    ],
                    "buttontrial" : [
                        "true"
                    ],
                    "assessmentsession" : [
                        {
                            "conditiontemplatesbygroup" : [
                                {
                                    "groupnames" : [
                                        "A"
                                    ],
                                    "clustersrepeated" : [
                                        "6"
                                    ],
                                    "templatesrepeated" : [
                                        "12"
                                    ],
                                    "group" : [
                                        "0,f,t,0 1,f,t,1 2,f,t,2 0,f,t,36 1,f,t,37 2,f,t,38\n                       0,f,t,0 1,f,t,1 2,f,t,2 0,f,t,36 1,f,t,37 2,f,t,38\n                       0,f,t,0 1,f,t,1 2,f,t,2 0,f,t,36 1,f,t,37 2,f,t,38\n                       0,f,t,0 1,f,t,1 2,f,t,2 0,f,t,36 1,f,t,37 2,f,t,38\n                       0,f,t,0 1,f,t,1 2,f,t,2 0,f,t,36 1,f,t,37 2,f,t,38\n                       0,f,t,0 1,f,t,1 2,f,t,2 0,f,t,36 1,f,t,37 2,f,t,38\n                       0,f,t,0 1,f,t,1 2,f,t,2 0,f,t,36 1,f,t,37 2,f,t,38\n                       0,f,t,0 1,f,t,1 2,f,t,2 0,f,t,36 1,f,t,37 2,f,t,38\n                       0,f,t,0 1,f,t,1 2,f,t,2 0,f,t,36 1,f,t,37 2,f,t,38\n                       0,f,t,0 1,f,t,1 2,f,t,2 0,f,t,36 1,f,t,37 2,f,t,38\n                       0,f,t,0 1,f,t,1 2,f,t,2 0,f,t,36 1,f,t,37 2,f,t,38\n                       0,f,t,0 1,f,t,1 2,f,t,2 0,f,t,36 1,f,t,37 2,f,t,38\n                "
                                    ]
                                }
                            ],
                            "initialpositions" : [
                                "\n                A_1 A_1 A_1\n                A_2 A_2 A_2\n                A_3 A_3 A_3\n                A_4 A_4 A_4\n                A_5 A_5 A_5\n                A_6 A_6 A_6\n                A_7 A_7 A_7\n                A_8 A_8 A_8\n                A_9 A_9 A_9\n                A_10 A_10 A_10\n                A_11 A_11 A_11\n                A_12 A_12 A_12\n\n                A_1 A_1 A_1\n                A_2 A_2 A_2\n                A_3 A_3 A_3\n                A_4 A_4 A_4\n                A_5 A_5 A_5\n                A_6 A_6 A_6\n                A_7 A_7 A_7\n                A_8 A_8 A_8\n                A_9 A_9 A_9\n                A_10 A_10 A_10\n                A_11 A_11 A_11\n                A_12 A_12 A_12\n                     "
                            ],
                            "randomizegroups" : [
                                "false"
                            ],
                            "clusterlist" : [
                                "0-11"
                            ],
                            "assignrandomclusters" : [
                                "false"
                            ],
                            "permutefinalresult" : [
                                "0-35 36-71"
                            ]
                        }
                    ]
                },
                {
                    "unitname" : [
                        "Musical Interval Study Practice Harmony1"
                    ],
                    "unitinstructions" : [
                        "Training portion (108 practices)\n\n            Listen to each sound and indicate which musical interval you just heard.\n        "
                    ],
                    "deliveryparams" : [
                        {
                            "purestudy" : [
                                "3000"
                            ],
                            "readyprompt" : [
                                "0"
                            ],
                            "reviewstudy" : [
                                "3000"
                            ],
                            "correctprompt" : [
                                "1000"
                            ],
                            "drill" : [
                                "120000"
                            ]
                        }
                    ],
                    "buttontrial" : [
                        "true"
                    ],
                    "assessmentsession" : [
                        {
                            "conditiontemplatesbygroup" : [
                                {
                                    "groupnames" : [
                                        "A B C"
                                    ],
                                    "clustersrepeated" : [
                                        "18 18 18"
                                    ],
                                    "templatesrepeated" : [
                                        "2 2 2"
                                    ],
                                    "group" : [
                                        "r,f,t,0  r,f,t,1  r,f,t,2\n                       r,f,t,30 r,f,t,31 r,f,t,32\n                       r,f,t,42 r,f,t,43 r,f,t,44\n                       r,f,t,54 r,f,t,55 r,f,t,56\n                       r,f,t,84 r,f,t,85 r,f,t,86\n                       r,f,t,96 r,f,t,97 r,f,t,98\n\n                       r,f,t,0  r,f,t,1  r,f,t,2\n                       r,f,t,30 r,f,t,31 r,f,t,32\n                       r,f,t,42 r,f,t,43 r,f,t,44\n                       r,f,t,54 r,f,t,55 r,f,t,56\n                       r,f,t,84 r,f,t,85 r,f,t,86\n                       r,f,t,96 r,f,t,97 r,f,t,98\n                ",
                                        "r,f,t,0 r,f,t,1 r,f,t,2 r,f,t,3 r,f,t,4 r,f,t,5\n                       r,f,t,30 r,f,t,31 r,f,t,32 r,f,t,33 r,f,t,34 r,f,t,35\n                       r,f,t,42 r,f,t,43 r,f,t,44 r,f,t,45 r,f,t,46 r,f,t,47\n\n                       r,f,t,0 r,f,t,1 r,f,t,2 r,f,t,3 r,f,t,4 r,f,t,5\n                       r,f,t,30 r,f,t,31 r,f,t,32 r,f,t,33 r,f,t,34 r,f,t,35\n                       r,f,t,42 r,f,t,43 r,f,t,44 r,f,t,45 r,f,t,46 r,f,t,47\n                ",
                                        "r,f,t,0 r,f,t,1 r,f,t,2 r,f,t,3 r,f,t,4 r,f,t,5\n                       r,f,t,6 r,f,t,7 r,f,t,8 r,f,t,9 r,f,t,10 r,f,t,11\n                       r,f,t,12 r,f,t,13 r,f,t,14 r,f,t,15 r,f,t,16 r,f,t,17\n\n                       r,f,t,0 r,f,t,1 r,f,t,2 r,f,t,3 r,f,t,4 r,f,t,5\n                       r,f,t,6 r,f,t,7 r,f,t,8 r,f,t,9 r,f,t,10 r,f,t,11\n                       r,f,t,12 r,f,t,13 r,f,t,14 r,f,t,15 r,f,t,16 r,f,t,17\n                "
                                    ]
                                }
                            ],
                            "randomchoices" : [
                                "2"
                            ],
                            "initialpositions" : [
                                "\n                A_1 A_1 A_1 A_2 A_2 A_2\n                B_1 B_1 B_1 B_1 B_1 B_1\n                C_1 C_1 C_1 C_1 C_1 C_1\n                C_1 C_1 C_1 C_1 C_1 C_1\n                C_1 C_1 C_1 C_1 C_1 C_1\n\n                A_1 A_1 A_1 A_2 A_2 A_2\n                B_1 B_1 B_1 B_1 B_1 B_1\n\n                A_1 A_1 A_1 A_2 A_2 A_2\n                B_1 B_1 B_1 B_1 B_1 B_1\n\n                A_1 A_1 A_1 A_2 A_2 A_2\n                B_2 B_2 B_2 B_2 B_2 B_2\n                C_2 C_2 C_2 C_2 C_2 C_2\n                C_2 C_2 C_2 C_2 C_2 C_2\n                C_2 C_2 C_2 C_2 C_2 C_2\n\n                A_1 A_1 A_1 A_2 A_2 A_2\n                B_2 B_2 B_2 B_2 B_2 B_2\n\n                A_1 A_1 A_1 A_2 A_2 A_2\n                B_2 B_2 B_2 B_2 B_2 B_2\n            "
                            ],
                            "randomizegroups" : [
                                "false"
                            ],
                            "clusterlist" : [
                                "0-5"
                            ],
                            "assignrandomclusters" : [
                                "true"
                            ],
                            "permutefinalresult" : [
                                "0-29 30-41 42-53 54-83 84-95\n                96-107"
                            ]
                        }
                    ]
                },
                {
                    "unitinstructions" : [
                        "You have completed Session 1 of the experiment. Please enter the code- Siberian husky -into the Amazon Mechanical Turk website for this HIT and submit your response. \n        \n        If the countdown below is 2 minutes: Please wait for the countdown to finish and take the posttest now.\n        \n        If the countdown below is 1 day or 7 days: No later than tomorrow morning, you will receive a private message (delivered to your email inbox) for instructions on Session 2. "
                    ],
                    "unitname" : [
                        "Session 1 end"
                    ],
                    "deliveryparams" : [
                        {
                            "lockoutminutes" : [
                                "2"
                            ]
                        },
                        {
                            "lockoutminutes" : [
                                "1440"
                            ]
                        },
                        {
                            "lockoutminutes" : [
                                "10080"
                            ]
                        }
                    ]
                },
                {
                    "unitname" : [
                        "Musical Interval Study Posttest Harmony1"
                    ],
                    "unitinstructions" : [
                        "Posttest portion (72 sounds)\n\n            Listen to each sound, then click on the name of the interval you just heard. After a correct response, you will be shown the next interval. After each incorrect response, you will be provided with review to help you learn.\n        "
                    ],
                    "deliveryparams" : [
                        {
                            "purestudy" : [
                                "3000"
                            ],
                            "readyprompt" : [
                                "0"
                            ],
                            "reviewstudy" : [
                                "3000"
                            ],
                            "correctprompt" : [
                                "1000"
                            ],
                            "drill" : [
                                "10000"
                            ]
                        }
                    ],
                    "buttontrial" : [
                        "true"
                    ],
                    "assessmentsession" : [
                        {
                            "conditiontemplatesbygroup" : [
                                {
                                    "groupnames" : [
                                        "A"
                                    ],
                                    "clustersrepeated" : [
                                        "6"
                                    ],
                                    "templatesrepeated" : [
                                        "12"
                                    ],
                                    "group" : [
                                        "0,f,t,0 1,f,t,1 2,f,t,2 0,f,t,36 1,f,t,37 2,f,t,38\n                       0,f,t,0 1,f,t,1 2,f,t,2 0,f,t,36 1,f,t,37 2,f,t,38\n                       0,f,t,0 1,f,t,1 2,f,t,2 0,f,t,36 1,f,t,37 2,f,t,38\n                       0,f,t,0 1,f,t,1 2,f,t,2 0,f,t,36 1,f,t,37 2,f,t,38\n                       0,f,t,0 1,f,t,1 2,f,t,2 0,f,t,36 1,f,t,37 2,f,t,38\n                       0,f,t,0 1,f,t,1 2,f,t,2 0,f,t,36 1,f,t,37 2,f,t,38\n                       0,f,t,0 1,f,t,1 2,f,t,2 0,f,t,36 1,f,t,37 2,f,t,38\n                       0,f,t,0 1,f,t,1 2,f,t,2 0,f,t,36 1,f,t,37 2,f,t,38\n                       0,f,t,0 1,f,t,1 2,f,t,2 0,f,t,36 1,f,t,37 2,f,t,38\n                       0,f,t,0 1,f,t,1 2,f,t,2 0,f,t,36 1,f,t,37 2,f,t,38\n                       0,f,t,0 1,f,t,1 2,f,t,2 0,f,t,36 1,f,t,37 2,f,t,38\n                       0,f,t,0 1,f,t,1 2,f,t,2 0,f,t,36 1,f,t,37 2,f,t,38\n                "
                                    ]
                                }
                            ],
                            "initialpositions" : [
                                "\n                A_1 A_1 A_1\n                A_2 A_2 A_2\n                A_3 A_3 A_3\n                A_4 A_4 A_4\n                A_5 A_5 A_5\n                A_6 A_6 A_6\n                A_7 A_7 A_7\n                A_8 A_8 A_8\n                A_9 A_9 A_9\n                A_10 A_10 A_10\n                A_11 A_11 A_11\n                A_12 A_12 A_12\n\n                A_1 A_1 A_1\n                A_2 A_2 A_2\n                A_3 A_3 A_3\n                A_4 A_4 A_4\n                A_5 A_5 A_5\n                A_6 A_6 A_6\n                A_7 A_7 A_7\n                A_8 A_8 A_8\n                A_9 A_9 A_9\n                A_10 A_10 A_10\n                A_11 A_11 A_11\n                A_12 A_12 A_12\n\n            "
                            ],
                            "randomizegroups" : [
                                "false"
                            ],
                            "clusterlist" : [
                                "0-11"
                            ],
                            "assignrandomclusters" : [
                                "false"
                            ],
                            "permutefinalresult" : [
                                "0-35 36-71"
                            ]
                        }
                    ]
                },
                {
                    "unitname" : [
                        "last"
                    ],
                    "unitinstructions" : [
                        "\n            You have now completed the posttest. Please complete the survey located here\n            <a href='https://docs.google.com/forms/d/1q020_MDsqTo88PMUFs6pvZ3QmyLHyR35E5pa96-M0B4/viewform?usp=send_form'\n               target=\"_blank\"\n               >https://docs.google.com/forms/d/1q020_MDsqTo88PMUFs6pvZ3QmyLHyR35E5pa96-M0B4/viewform?usp=send_form</a>.\n            We will evaluate your response within a few days for consideration of the $3 bonus.  "
                    ]
                }
            ]
        }
    }
};
};

MusicStim = function() { return {
    "_id" : "zBfWa2GCvY9iadzfG",
    "fileName" : "Music2stim.xml",
    "stimuli" : {
        "setspec" : {
            "clusters" : [
                {
                    "cluster" : [
                        {
                            "displayType" : [
                                "Sound"
                            ],
                            "display" : [
                                "music2/1",
                                "music2/7",
                                "music2/13"
                            ],
                            "response" : [
                                "Minor 3rd",
                                "Minor 3rd",
                                "Minor 3rd"
                            ]
                        },
                        {
                            "displayType" : [
                                "Sound"
                            ],
                            "display" : [
                                "music2/2",
                                "music2/8",
                                "music2/14"
                            ],
                            "response" : [
                                "Major 3rd",
                                "Major 3rd",
                                "Major 3rd"
                            ]
                        },
                        {
                            "displayType" : [
                                "Sound"
                            ],
                            "display" : [
                                "music2/3",
                                "music2/9",
                                "music2/15"
                            ],
                            "response" : [
                                "Tritone",
                                "Tritone",
                                "Tritone"
                            ]
                        },
                        {
                            "displayType" : [
                                "Sound"
                            ],
                            "display" : [
                                "music2/4",
                                "music2/10",
                                "music2/16"
                            ],
                            "response" : [
                                "Perfect 5th",
                                "Perfect 5th",
                                "Perfect 5th"
                            ]
                        },
                        {
                            "displayType" : [
                                "Sound"
                            ],
                            "display" : [
                                "music2/5",
                                "music2/11",
                                "music2/17"
                            ],
                            "response" : [
                                "Major 7th",
                                "Major 7th",
                                "Major 7th"
                            ]
                        },
                        {
                            "displayType" : [
                                "Sound"
                            ],
                            "display" : [
                                "music2/6",
                                "music2/12",
                                "music2/18"
                            ],
                            "response" : [
                                "Octave",
                                "Octave",
                                "Octave"
                            ]
                        },
                        {
                            "displayType" : [
                                "Sound"
                            ],
                            "display" : [
                                "music2/19",
                                "music2/25",
                                "music2/31"
                            ],
                            "response" : [
                                "Minor 3rd",
                                "Minor 3rd",
                                "Minor 3rd"
                            ]
                        },
                        {
                            "displayType" : [
                                "Sound"
                            ],
                            "display" : [
                                "music2/20",
                                "music2/26",
                                "music2/32"
                            ],
                            "response" : [
                                "Major 3rd",
                                "Major 3rd",
                                "Major 3rd"
                            ]
                        },
                        {
                            "displayType" : [
                                "Sound"
                            ],
                            "display" : [
                                "music2/21",
                                "music2/27",
                                "music2/33"
                            ],
                            "response" : [
                                "Tritone",
                                "Tritone",
                                "Tritone"
                            ]
                        },
                        {
                            "displayType" : [
                                "Sound"
                            ],
                            "display" : [
                                "music2/22",
                                "music2/28",
                                "music2/34"
                            ],
                            "response" : [
                                "Perfect 5th",
                                "Perfect 5th",
                                "Perfect 5th"
                            ]
                        },
                        {
                            "displayType" : [
                                "Sound"
                            ],
                            "display" : [
                                "music2/23",
                                "music2/29",
                                "music2/35"
                            ],
                            "response" : [
                                "Major 7th",
                                "Major 7th",
                                "Major 7th"
                            ]
                        },
                        {
                            "displayType" : [
                                "Sound"
                            ],
                            "display" : [
                                "music2/24",
                                "music2/30",
                                "music2/36"
                            ],
                            "response" : [
                                "Octave",
                                "Octave",
                                "Octave"
                            ]
                        }
                    ]
                }
            ]
        }
    }
};
};
